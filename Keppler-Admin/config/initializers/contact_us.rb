# Agregar datos de configuración
KepplerContactUs.setup do |config|
  config.mailer_to = 'contacto@reliabilitymediagroup.com'
  config.mailer_from = 'contacto@reliabilitymediagroup.com'
  config.name_web = 'Reliability'
  # Route redirection after send
  config.redirection = 'http://www.reliabilitymediagroup.com/#contacts'

  # Agregar keys de google recaptcha
  Recaptcha.configure do |recatcha|
    recatcha.public_key  = '6LdzoiATAAAAAOSfjk1wrOWGkB8i8fAkCfz7KnBX'
    recatcha.private_key = '6LdzoiATAAAAACUjVHJqYB7em8zP5b5Om9boibVd'
    recatcha.api_version = 'v2'
  end
end
