class FrontendController < ApplicationController
  layout 'layouts/frontend/application'

  def landing
    @message = KepplerContactUs::Message.new
    @services = KepplerCatalogs::Catalog
                .find_by(name: 'Servicios').attachments.all
    @clients = KepplerCatalogs::Catalog
               .find_by(name: 'Clientes').attachments.all
    @comments = KepplerCatalogs::Catalog
                .find_by(name: 'Comentarios').attachments.all
    @posts = KepplerBlog::Post
             .where(public: true).order('created_at DESC').limit(6)
  end
end
