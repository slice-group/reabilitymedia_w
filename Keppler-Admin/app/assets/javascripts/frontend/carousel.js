$(document).ready(function(){
	// --Progress bars that triggers changes of slides on Home
	$('.current-home').on('webkitAnimationIteration', function(event) {
		$('.home-carousel').slick("slickNext");
	});

	// --Progress bars that triggers changes of slides on Quotes
	$('.current-quotes').on('webkitAnimationIteration', function(event) {
		$('.quotes').slick("slickNext");
	});

	// -- Home --
	$('.home-carousel').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToSroll: 1,
		arrows: false,
		dots: false,
		draggable: false
	});

	// -- About Us
	$('.quotes').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToSroll: 1,
		arrows: false,
		dots: false,
		draggable: false,
		responsive: [
			{
				breakpoint: 990,
				settings: {
					dots: true
				}
			}
		]
	});

	// -- Blog --
	$('.blog-gallery').slick({
		infinite: false,
		slidesToShow: 2,
		slidesToSroll: 1,
		arrows: false,
		dots: true,
		draggable: false,
		autoplay: false,
		responsive: [
			{
				breakpoint: 990,
				settings: {
					dots: true,
					infinite: true,
					slidesToShow: 1,
					slidesToScroll:1
				}
			},
			{
				breakpoint: 600,
				settings: {
					dots: true,
					infinite: true,
					slidesToShow: 1,
					slidesToScroll:1
				}
			},
			{
				breakpoint: 480,
				settings: {
					dots: true,
					infinite: true,
					slidesToShow: 1,
					slidesToScroll:1
				}
			}
		]
	});

	// -- Clients
	$('.clients-gallery').slick({
		infinite: true,
		pauseOnHover: false,
		slidesToShow: 4,
		arrows: false,
		dots: false,
		autoplay: true,
		autoplaySpeed: 5000,
		responsive: [
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 2
				}
			}
		]
	});

});
