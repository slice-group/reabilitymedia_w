function removeHash () {
	history.pushState("", document.title, window.location.pathname + window.location.search);
}

function scrollToAnchor(anchor){
	$('#'+anchor).animatescroll({
		scrollSpeed:1500
	})
	removeHash ();
}

$( document ).ready(function() {

	var sections = $('section'), nav = $('.navbar-nav'), nav_height = nav.outerHeight();

	$(window).on('scroll', function () {
		var cur_pos = $(this).scrollTop();

		sections.each(function() {
			var top = $(this).offset().top - 60,
				bottom = top + $(this).outerHeight();

			if (cur_pos >= top && cur_pos <= bottom) {
				nav.find('li').removeClass('current');
				sections.removeClass('current');

				$(this).addClass('current');
				nav.find('#link-'+$(this).attr('id')).addClass('current');
			}
		});
	});



});
$( document ).ready(function() {
	navbarColor()
});
document.onscroll = function() {
	navbarColor()
}
function navbarColor() {
	if($(window).scrollTop() > $('#home').height()-60){
		$('#front-navbar').removeClass('home-navbar-color')
		$('#brand-logo').attr('src', '/assets/frontend/logo_negro.png')
	}else{
		$('#front-navbar').addClass('home-navbar-color')
		$('#brand-logo').attr('src', '/assets/frontend/logo_blanco.png')
	}
}

$(document).click(function (event) {
	var clickover = $(event.target);
	if ( $(".navbar-collapse").hasClass("in") === true && !clickover.hasClass("navbar-toggle")) {
		$(".navbar-collapse").collapse('hide');
	}
});