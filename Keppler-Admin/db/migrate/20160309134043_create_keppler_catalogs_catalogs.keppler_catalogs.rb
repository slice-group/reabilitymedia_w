class CreateKepplerCatalogsCatalogs < ActiveRecord::Migration
  def change
    create_table :keppler_catalogs_catalogs do |t|
      t.string :name
      t.text :description
      t.string :permalink

      t.timestamps null: false
    end
  end
end
