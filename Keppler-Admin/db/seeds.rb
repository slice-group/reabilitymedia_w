# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# user = CreateAdminService.new.call
# puts 'CREATED ADMIN USER: ' << user.email

[:admin].each do |name|
  Role.create name: name
  puts "#{name} creado"
end

User.create name: 'Admin',
            email: 'admin@keppler.com',
            password: '12345678',
            password_confirmation: '12345678',
            role_ids: '1'
puts 'admin@keppler.com ha sido creado'

KepplerCatalogs::Catalog.create name: "Clientes", description: "Iconos de clientes"
puts 'Clientes ha sido creado'

KepplerCatalogs::Catalog.create name: "Comentarios", description: "Comentarios acerca de nosotros"
puts 'Comentarios ha sido creado'

KepplerCatalogs::Catalog.create name: "Servicios", description: "Información de servicios que ofrecemos"
puts 'Servicios ha sido creado'

puts '---- CATALOGOS CREADOS ----'

KepplerCatalogs::Attachment.create(
  name: 'Fashion Digital Marketing', public: true, catalog_id: 3, aditional_text: '',
  description: 'Empleando técnicas y una filosofía de negocios centradas en el consumidor, analizamos el cliente, mejoramos la comunicación interna de tu negocio y contribuimos a reducir los costos del mismo, ampliando así la audiencia y el valor de tu marca en el mercado.'
)
puts 'Fashion Digital Marketing, Creado'

KepplerCatalogs::Attachment.create(
  name: 'Fashion Marketing Research', public: true, catalog_id: 3, aditional_text: '',
  description: 'Elaboramos estudios e investigación de mercados en la industria de la moda, para determinar la dimensión del mismo, la competencia, las tendencias de la moda en el exterior, la comercialización de bienes y servicios, el desarrollo de productos, el precio y la publicidad.'
)
puts 'Fashion Digital Research, Creado'

KepplerCatalogs::Attachment.create(
  name: 'Fashion Marketing Communications', public: true, catalog_id: 3, aditional_text: '',
  description: 'Elaboramos la estrategia de comunicación más influyente y significativa, basándonos en la información que conduce el mensaje, a través de comunicados de prensa, revistas, diarios comerciales, desfiles de moda,semanas de la moda, showrooms, exposiciones, sitios web y redes sociales.'
)
puts 'Fashion Digital Communications, Creado'

KepplerCatalogs::Attachment.create(
  name: 'Fashion Social Networks', public: true, catalog_id: 3, aditional_text: '',
  description: 'El crecimiento e innovación de las herramientas digitales ha hecho que resulte difícil escoger el medio social más adecuado para interactuar con el impulsamos la consolidación de tu marca seleccionando las mejores plataformas de redes sociales en el consumidor; es por ello que ámbito de la moda.'
)
puts 'Fashion Social Networks, Creado'

KepplerCatalogs::Attachment.create(
  name: 'Fashion Mobile Marketing', public: true, catalog_id: 3, aditional_text: '',
  description: 'Siendo el teléfono celular una herramienta personal y fundamental para el ser humano,desarrollamos aplicaciones para smartphones y tablets que impulsen y refuercen tu marca, satisfaciendo las necesidades del cliente.'
)
puts 'Fashion Mobile Marketing, Creado'

KepplerCatalogs::Attachment.create(
  name: 'Fashion Public Relations (FPR)', public: true, catalog_id: 3, aditional_text: '',
  description: 'Diseñamos estrategias de relaciones públicas, que garantizarán que los productos trendy de tu marca, brinden prestigio y notoriedad en el dominio público a través de los medios de comunicación.'
)
puts 'Fashion Public Relations (FPR), Creado'

KepplerCatalogs::Attachment.create(
  name: 'Celebrity Management', public: true, catalog_id: 3, aditional_text: '',
  description: 'Las celebridades son una marca, y por ello gestionamos, sustentamos y protegemos su valor en el mercado a través de estudios del ciclo de vida de la celebridad cómo producto y estrategias de campañas de las marcas de moda.'
)
puts 'Celebrity Management, Creado'

KepplerCatalogs::Attachment.create(
  name: 'Fashion E-commerce site', public: true, catalog_id: 3, aditional_text: '',
  description: 'El e-commerce del mundo de la moda ya no se basa únicamente en precios, variedad y conveniencia. Se trata de colocar el producto adecuado a través de una experiencia de compra positiva para el consumidor. Desarrollar websites que otorguen productividad, usabilidad y fiabilidad es nuestro objetivo, conectando así con el proceso de decisión de compra de consumidor.'
)
puts 'Fashion E-commerce site, Creado'

KepplerCatalogs::Attachment.create(
  name: 'Retail Fashion Store Management', public: true, catalog_id: 3, aditional_text: '',
  description: 'Desarrollamos metodologías dirigidas a las tiendas minoristas, asociadas al comportamiento del consumidor, comercialización visual y a los medios de comunicación del marketing.'
)
puts 'Retail Fashion Store Management, Creado'

KepplerCatalogs::Attachment.create(
  name: 'Fashion Event Planning', public: true, catalog_id: 3, aditional_text: '',
  description: 'Las oportunidades en el mundo de la moda se presentan en las temporadas de los Fashion Shows, Fashion Weeks, Showrooms, Exhibiciones, Tradeshows, Trunk Shows y Fashion Brand Stands, mercadeando así la tendencia del momento, otorgando así al comprador el conocimiento del producto. Por ello, nos encargamos de crear un canal de comunicación entre la marca y el consumidor, planificando la logística y desarrollando la producción del evento.'
)
puts 'Fashion Event Planning, Creado'

puts '---- SERVICIOS CREADOS ----'

KepplerCatalogs::Attachment.create(
  name: '¡Neuromarketers!', catalog_id: 2, public: true,
  description: 'Reliability Media evoca fiabilidad en los servicios e información que compartimos con los "prosumidores" del mundo de la moda.',
  aditional_text: 'Luis E. Vielma - Director General de Reliability Media: - Neuromarketeer. Asesor de fashion marketing, especialista en Neuropublicidad, egresado de la Universidad Comercial Luigi Bocconi.'
)
puts 'Primer comentario creado'