#Generado por keppler
require 'elasticsearch/model'
module KepplerCatalogs
  class Catalog < ActiveRecord::Base
    include Elasticsearch::Model
    include Elasticsearch::Model::Callbacks
    before_save :create_permalink
    has_many :attachments, :dependent => :destroy
    validates_presence_of :name
    validates_uniqueness_of :name
    validates_length_of :name, :minimum => 1, :maximum => 15

    after_commit on: [:update] do
      puts __elasticsearch__.index_document
    end

    def self.searching(query)
      if query
        self.search(self.query query).records.order(id: :desc)
      else
        self.order(id: :desc)
      end
    end

    def self.query(query)
      { query: { multi_match:  { query: query, fields: [:name, :description] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
    end

    def as_indexed_json(options={})
      {
        id: self.id.to_s,
        name:  self.name.to_s,
        description: ActionView::Base.full_sanitizer.sanitize(self.description, tags: [])
      }.as_json
    end

    def published_files
      self.attachments.where(public: true)
    end

    def published_files_count
      self.attachments.where(public: true).count
    end

    private

    def create_permalink
      self.permalink = self.name.downcase.parameterize
    end

  end
  #Catalog.import
end
