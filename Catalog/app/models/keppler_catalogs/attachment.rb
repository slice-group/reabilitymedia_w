#Generado por keppler
require 'elasticsearch/model'
module KepplerCatalogs
  class Attachment < ActiveRecord::Base
    include Elasticsearch::Model
    include Elasticsearch::Model::Callbacks
    belongs_to :catalog
    before_save :create_permalink
    mount_uploader :image, CoverUploader
    validates_uniqueness_of :name
    validates_presence_of :name


    after_commit on: [:update] do
      puts __elasticsearch__.index_document
    end

    def self.searching(query)
      if query
        self.search(self.query query).records.order(id: :desc)
      else
        self.order(id: :desc)
      end
    end

    def self.query(query)
      { query: { multi_match:  { query: query, fields: [:name, :description, :public] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
    end

    #armar indexado de elasticserch
    def as_indexed_json(options={})
      {
        id: self.id.to_s,
        name:  self.name,
        description: ActionView::Base.full_sanitizer.sanitize(self.description, tags: []),
        public:  self.public ? "publicado" : "no--publicado"
      }.as_json
    end

    def create_permalink
      self.permalink = self.name.downcase.parameterize
    end

    def remove_image_public
      if !self.image_url.nil? and self.upload != "1"
        image = File.dirname(Rails.root.join("public"+self.image_url))
        FileUtils.rm_rf(image)
      end
    end
  end
  #Attachment.import
end
