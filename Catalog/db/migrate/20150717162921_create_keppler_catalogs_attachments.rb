# This migration comes from keppler_catalogs (originally 20150717162921)
class CreateKepplerCatalogsAttachments < ActiveRecord::Migration
  def change
    create_table :keppler_catalogs_attachments do |t|
      t.string :name
      t.text :description
      t.text :aditional_text
      t.string :image
      t.boolean :public
      t.string :permalink
      t.belongs_to :catalog

      t.timestamps null: false
    end
  end
end
